from random import randint


# Asks your name with the prompt "Hi! What is your name?"
# Then, it does the following up to five times:

# Tries to guess your birth month and year with a prompt formatted like Guess «guess number» : «name» were you born in «m» / «yyyy» ? then prompts with "yes or no?"
# If the computer guesses it correctly because you respond yes, it prints the message I knew it! and stops guessing
# If the computer guesses incorrectly because you respond no, it prints the message Drat! Lemme try again! if it's only guessed 1, 2, 3, or 4 times. Otherwise, it prints the message I have other things to do. Good bye.
# It should only guess years between 1924 and 2004 including those years.

# create input variable with prompt for name 



# first guess 
# assign month variable to randint 1-12 
# assign year variable to randint 1924-2004
# prints statement guess 1 were you born in month variable / year variable 
# define response variable with yes or no question 

# if reponse is yes
#     print confirmation
# else:
#     print try again
    
#     copy and paste this 5 times cause we cant loop it yet and the guess number is hard coded in.


name = input("Hi! what is your name? ")



for num in range(1, 6):
    month = randint(1, 12)
    year = randint(1924, 2004)
    print(f'Guess {num} : ', name, ' were you born in ', month, "/", year, "?")
    response = input('yes or no? ')
    #not accounting for outlier cases
    if response == 'yes':
        print('I knew it')
        exit()
    elif num == 5:
        print('I have other things to do, pound sand')
    else:
        print('Let me try again')
    